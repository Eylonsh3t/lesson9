#include "BSNode.h"

BSNode::BSNode(std::string data)
{
	this->_data = data;
	this->_left = NULL;
	this->_right = NULL;
	this->_count = 1;
}

BSNode::BSNode(const BSNode& other)
{
	this->operator=(other);
}

BSNode::~BSNode()
{
}

void BSNode::deleteTree(BSNode *node)
{
	if (node)
	{
		deleteTree(node->_left);
		deleteTree(node->_right);
		node = NULL;
	}
}

void BSNode::insert(std::string value)
{
	if (this->_data == value)
	{
		this->_count++;
	}
	if (this->_data > value)
	{
		if (this->_left != NULL)
		{
			this->_left->insert(value);
		}
		else
		{
			this->_left = new BSNode(value);
		}
	}
	else if (this->_data < value)
	{
		if (this->_right != NULL)
		{
			this->_right->insert(value);
		}
		else
		{
			this->_right = new BSNode(value);
		}
	}
}

BSNode& BSNode::operator=(const BSNode& other)
{
	BSNode bs = BSNode(other.getData());
	bs._left = other._left;
	bs._right = other._right;
	bs._count = other._count;
	return *this;
}

bool BSNode::isLeaf() const
{
	if (this->_left == NULL && this->_right == NULL)
	{
		return false;
	}
	else
	{
		return true;
	}
}

std::string BSNode::getData() const
{
	return this->_data;
}

BSNode* BSNode::getLeft() const
{
	return this->_left;
}

BSNode* BSNode::getRight() const
{
	return this->_right;
}

int BSNode::getCount() const
{
	return this->_count;
}

bool BSNode::search(std::string val) const
{
	if (val == this->_data)
	{
		return true;
	}
	if (val < this->_data)
	{
		if (this->_left)
		{
			return search(this->_left->_data);
		}
		else
		{
			return false;
		}
	}
	if (val > this->_data)
	{
		if (this->_right)
		{
			return search(this->_right->_data);
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

int BSNode::getHeight() const
{
	int rightHeight = - 1;
	int leftHeight = - 1;
	if (this->_left != NULL)
	{
		leftHeight = this->_left->getHeight();
	}
	if (this->_right != NULL)
	{
		rightHeight = this->_right->getHeight();
	}
	if (rightHeight > leftHeight)
	{
		return rightHeight + 1;
	}
	else
	{
		return leftHeight + 1;
	}
}

int BSNode::getDepth(const BSNode& root) const
{
	int depth = 0;
	if (this == NULL)
	{
		return -1;
	}
	if (root.getData() == this->_data)
	{
		return depth;
	}
	if (this->_data < root.getData())
	{
		if (root._left)
		{
			depth++;
			return findDepth(root._left, depth, this->_data);
		}
		else
		{
			return -1;
		}
	}
	if (this->_data > root.getData())
	{
		if (root._right)
		{
			depth++;
			return findDepth(root._right, depth, this->_data);
		}
		else
		{
			return -1;
		}
	}
	else
	{
		return -1;
	}
	return depth;
}

int BSNode::findDepth(BSNode* root, int depth, const std::string val) const
{
	if (root->getData() == val)
	{
		return depth;
	}
	if (val < root->getData())
	{
		if (root->_left)
		{
			depth++;
			return findDepth(root->_left, depth, val);
		}
		else
		{
			return -1;
		}
	}
	if (val > root->getData())
	{
		if (root->_right)
		{
			depth++;
			return findDepth(root->_right, depth, val);
		}
		else
		{
			return -1;
		}
	}
	else
	{
		return -1;
	}
}


void BSNode::printNodes() const
{
	this->inorder(this);
}

void BSNode::inorder(const BSNode* root) const //https://www.tutorialspoint.com/cplusplus-program-to-perform-inorder-recursive-traversal-of-a-given-binary-tree
{
	if (root != NULL)
	{
		inorder(root->_left);
		std::cout << root->_data << " ";
		std::cout << root->_count << std::endl;
		inorder(root->_right);
	}
}
