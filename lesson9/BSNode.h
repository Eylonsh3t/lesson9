#pragma once

#include <string>
#include <algorithm>
#include <iostream>

class BSNode
{
public:
	BSNode(std::string data);
	BSNode(const BSNode& other);

	~BSNode();
	void deleteTree(BSNode *node);
	void insert(std::string value);
	BSNode& operator=(const BSNode& other);

	bool isLeaf() const;
	std::string getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;
	int getCount() const;

	bool search(std::string val) const;

	int getHeight() const;
	int getDepth(const BSNode& root) const;
	int findDepth(BSNode* root, int depth, const std::string val) const;

	void printNodes() const; //for question 1 part C
	void inorder(const BSNode* root) const;

private:
	std::string _data;
	BSNode* _left;
	BSNode* _right;
	int _count;

	//int getCurrNodeDistFromInputNode(const BSNode* node) const; //auxiliary function for getDepth

};