#pragma once
#include <iostream>

class object
{
public:
	object(int num);
	~object();

	int _num;

	bool operator==(object other);
	bool operator>(object other);
	void operator=(object other);
	friend std::ostream& operator<<(std::ostream& os, object obj);
};