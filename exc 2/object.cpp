#include "object.h"

object::object(int num)
{
	this->_num = num;
}

object::~object()
{
}

bool object::operator==(object other)
{
	return (this->_num == other._num);
}

bool object::operator>(object other)
{
	return (this->_num > other._num);
}

void object::operator=(object other)
{
	this->_num = other._num;
}

std::ostream& operator<<(std::ostream& os, object obj)
{
	os << obj._num;
	return os;
}
