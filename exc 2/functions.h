#pragma once
#include "object.h"

template <class T>
int compare(T first, T second)
{
	if (first == second)
	{
		return 0;
	}
	else if (first > second)
	{
		return -1;
	}
	else
	{
		return 1;
	}
}

template <class T>
void bubbleSort(T arr[], const int size) //https://www.geeksforgeeks.org/cpp-program-for-bubble-sort/
{
	int i, j;
	bool swapped;
	for (i = 0; i < size - 1; i++)
	{
		swapped = false;
		for (j = 0; j < size - i - 1; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				int temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
				swapped = true;
			}
		}
		
		// IF no two elements were swapped by inner loop, then break 
		if (swapped == false)
			break;
	}
}

template <class T>
void printArray(T arr[], const int size)
{
	for (int i = 0; i < size; i++)
	{
		std::cout << arr[i] << " ";
	}
	std::cout << std::endl;
}