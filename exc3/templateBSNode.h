#pragma once

#include <string>
#include <algorithm>
#include <iostream>

template <class T>
class BSNode
{
public:
	BSNode(T data);
	BSNode(const BSNode& other);

	~BSNode();
	void deleteTree(BSNode* node);
	void insert(T value);
	BSNode& operator=(const BSNode& other);

	bool isLeaf() const;
	std::string getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;
	int getCount() const;

	bool search(std::string val) const;

	int getHeight() const;
	int getDepth(const BSNode& root) const;
	int findDepth(BSNode* root, int depth, const std::string val) const;
	void printNodes() const; //for question 1 part C
	void inorder(const BSNode* root) const;

private:
	T _data;
	BSNode* _left;
	BSNode* _right;
	int _count;

	//int getCurrNodeDistFromInputNode(const BSNode* node) const; //auxiliary function for getDepth

};

template <class T>
BSNode<T>::BSNode(T data)
{
	this->_data = data;
	this->_left = NULL;
	this->_right = NULL;
	this->_count = 1;
}

template <class T>
BSNode<T>::BSNode(const BSNode& other)
{
	this->operator=(other);
}

template <class T>
BSNode<T>::~BSNode()
{
}

template <class T>
void BSNode<T>::deleteTree(BSNode *node)
{
	if (node)
	{
		deleteTree(node->_left);
		deleteTree(node->_right);
		node = NULL;
	}
}

template <class T>
void BSNode<T>::insert(T value)
{
	if (this->_data == value)
	{
		this->_count++;
	}
	if (this->_data > value)
	{
		if (this->_left != NULL)
		{
			this->_left->insert(value);
		}
		else
		{
			this->_left = new BSNode(value);
		}
	}
	else if (this->_data < value)
	{
		if (this->_right != NULL)
		{
			this->_right->insert(value);
		}
		else
		{
			this->_right = new BSNode(value);
		}
	}
}

template <class T>
BSNode<T>& BSNode<T>::operator=(const BSNode& other)
{
	BSNode bs = BSNode(other.getData());
	bs._left = other._left;
	bs._right = other._right;
	bs._count = other._count;
	return *this;
}

template <class T>
bool BSNode<T>::isLeaf() const
{
	if (this->_left == NULL && this->_right == NULL)
	{
		return false;
	}
	else
	{
		return true;
	}
}

template <class T>
std::string BSNode<T>::getData() const
{
	return this->_data;
}

template <class T>
BSNode<T>* BSNode<T>::getLeft() const
{
	return this->_left;
}

template <class T>
BSNode<T>* BSNode<T>::getRight() const
{
	return this->_right;
}

template <class T>
int BSNode<T>::getCount() const
{
	return this->_count;
}

template <class T>
bool BSNode<T>::search(std::string val) const
{
	if (val == this->_data)
	{
		return true;
	}
	if (val < this->_data)
	{
		if (this->_left)
		{
			return search(this->_left->_data);
		}
		else
		{
			return false;
		}
	}
	if (val > this->_data)
	{
		if (this->_right)
		{
			return search(this->_right->_data);
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

template <class T>
int BSNode<T>::getHeight() const
{
	int rightHeight = - 1;
	int leftHeight = - 1;
	if (this->_left != NULL)
	{
		leftHeight = this->_left->getHeight();
	}
	if (this->_right != NULL)
	{
		rightHeight = this->_right->getHeight();
	}
	if (rightHeight > leftHeight)
	{
		return rightHeight + 1;
	}
	else
	{
		return leftHeight + 1;
	}
}

template <class T>
int BSNode<T>::getDepth(const BSNode& root) const
{
	int depth = 0;
	if (this == NULL)
	{
		return -1;
	}
	if (root.getData() == this->_data)
	{
		return depth;
	}
	if (this->_data < root.getData())
	{
		if (root._left)
		{
			depth++;
			return findDepth(root._left, depth, this->_data);
		}
		else
		{
			return -1;
		}
	}
	if (this->_data > root.getData())
	{
		if (root._right)
		{
			depth++;
			return findDepth(root._right, depth, this->_data);
		}
		else
		{
			return -1;
		}
	}
	else
	{
		return -1;
	}
	return depth;
}

template <class T>
int BSNode<T>::findDepth(BSNode* root, int depth, const std::string val) const
{
	if (root->getData() == val)
	{
		return depth;
	}
	if (val < root->getData())
	{
		if (root->_left)
		{
			depth++;
			return findDepth(root->_left, depth, val);
		}
		else
		{
			return -1;
		}
	}
	if (val > root->getData())
	{
		if (root->_right)
		{
			depth++;
			return findDepth(root->_right, depth, val);
		}
		else
		{
			return -1;
		}
	}
	else
	{
		return -1;
	}
}

template <class T>
void BSNode<T>::printNodes() const
{
	this->inorder(this);
}

template <class T>
void BSNode<T>::inorder(const BSNode* root) const //https://www.tutorialspoint.com/cplusplus-program-to-perform-inorder-recursive-traversal-of-a-given-binary-tree
{
	if (root != NULL)
	{
		inorder(root->_left);
		std::cout << root->_data << " ";
		std::cout << root->_count << std::endl;
		inorder(root->_right);
	}
}