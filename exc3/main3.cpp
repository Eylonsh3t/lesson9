#include "templateBSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>

int main()
{
	int size = 15;
	
	int arr1[15] = { 3, 2, 1, 7, 5, 6, 9, 8, 4, 10, 11, 14, 13, 15, 12 };
	
	std::string arr2[15] = { "c", "b", "a", "g", "e", "f", "d", "h", "j", "k", "i", "l", "n", "m", "o" };
	
	for (int i = 0; i < size; i++)
	{
		std::cout << arr1[i] << " ";
	}
	std::cout << "\n";
	BSNode<int>* cs = new BSNode<int>(arr1[0]);
	for (int i = 1; i < size; i++)
	{
		cs->insert(arr1[i]);
	}
	cs->printNodes();

	for (int i = 0; i < size; i++)
	{
		std::cout << arr2[i] << " ";
	}
	std::cout << "\n";
	BSNode<std::string>* as = new BSNode<std::string>(arr2[0]);
	for (int i = 1; i < size; i++)
	{
		as->insert(arr2[i]);
	}
	as->printNodes();

	cs->deleteTree(cs);
	as->deleteTree(as);

	delete cs;
	delete as;

	return 0;
}